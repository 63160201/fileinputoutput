/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.fileinputoutput;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author ASUS
 */
public class Computer implements Serializable{
    private int hand;
    private int playerHand;
    private int win,lose,draw;
    private int status;
    public Computer(){
        
    }
    private int choop(){
        return ThreadLocalRandom.current().nextInt(0,3);
    }
            
    public int paoYingChoop(int playerHand){
        this.playerHand = playerHand;
        this.hand = choop();
        if(this.playerHand == this.hand){
            status = 0;
            draw++;
            return 0;
        }
        if(this.playerHand == 0 && this.hand == 1){
            status = 1;
            win++;
            return 1;
        }
        if(this.playerHand == 1 && this.hand == 2){
            status = 1;
            win++;
            return 1;
        }
        if(this.playerHand == 2 && this.hand == 0){
            status = 1;
            win++;
            return 1;
        }
        lose++;
        status = -1;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }
    
    public int getStatus(){
        return status;
    }
}
